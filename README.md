This plugin can be used to generate a Linked Data description in JSON-LD format by writing JSDoc comments.

# Installation

Use the following command in the root directory where JSDoc is installed

```
npm install jsdoc-plugin-rdf
```

# Add the plugin to JSDoc

In JSDoc's configuration file add the path to the plugin

```
...
"plugins": [
  "node_modules/jsdoc-plugin-rdf"
],
```

If you want to print only the JSON-LD and no JSDoc documentation, you can change the JSDoc template in `opts.template` of JSDoc's configuration file. By using `plugin-rdf-template` only the JSON-LD will be printed to the path in `opts.destination`. Otherwise the JSON-LD will be generated along with the documentation and printed to a sub-folder as described under the output section.

```
...
"opts": {
  "destination": "./jsonld",
  "template": "node_modules/jsdoc-plugin-rdf/plugin-rdf-template"
}
```

# Usage

Generate a RDF triple with a subject, predicate and object tag. 

JSDoc comment:
```
@subj {type} id1 subject description
@pred predicate
@obj object
```

JSON-LD:
```
{
  "@context": {
    "schema": "https://schema.org/"
  },
  "@graph": [
    {
      "@id": "_:id1",
      "@type": "type",
      "schema:description": "subject description",
      "predicate": "object"
    }
  ]
}
```

The object position can have type and description too, but it will change the object string from a property value to a graph node id for the object description

```
@subj {subjectType} id2 subject description
@pred predicate
@obj {objectType} object the object description
```
```
{
  "@context": {
    "schema": "https://schema.org/"
  },
  "@graph": [
    {
      "@id": "_:id2",
      "@type": "subjectType",
      "schema:description": "subject description",
      "predicate": {
        "@id": "_:object"
      }
    },
    {
      "@id": "_:object",
      "@type": "objectType",
      "schema:description": "the object description"
    }
  ]
}
```

# Examples
An example `jsdoc.config.json` with a playground `js` file and its `jsonld` result can be found in the [examples](https://gitlab.com/dBPMS-PROCEED/jsdoc-plugin-rdf/tree/master/examples) directory. With JSDoc installed globally, you can run

```
jsdoc -c jsdoc.config.json
```

in the examples directory to test your own descriptions added to the `js` folder. Rename the [EXAMPLE.rdf.config.json](https://gitlab.com/dBPMS-PROCEED/jsdoc-plugin-rdf/tree/master) to `rdf.config.json` to try the settings explained below.

The example of the JSON-LD [Playground](https://json-ld.org/playground/#startTab=tab-compacted&json-ld=%7B%22%40context%22%3A%7B%22%40vocab%22%3A%22https%3A%2F%2Fschema.org%2F%22%2C%22firstname%22%3A%22givenName%22%2C%22lastname%22%3A%22familyName%22%2C%22birthDay%22%3A%22birthDate%22%2C%22id%22%3A%22%40id%22%7D%2C%22id%22%3A%2242%22%2C%22firstname%22%3A%22John%22%2C%22lastname%22%3A%22Doe%22%2C%22birthDay%22%3A%221978-05-17%22%7D&context=%7B%22%40vocab%22%3A%22https%3A%2F%2Fschema.org%2F%22%2C%22first_name%22%3A%22givenName%22%2C%22last_name%22%3A%22familyName%22%2C%22birth_date%22%3A%22birthDate%22%2C%22person_id%22%3A%22%40id%22%7D) can be generated with the following JSDoc comment.

playground.js

```
/**
@subj {schema:Person} 42
@pred schema:givenName
@obj John
@pred schema:familyName
@obj Doe
@pred schema:birthDate
@obj 1978-05-17
*/
```

JSON-LD result:

```
{
  "@context": {
    "schema": "https://schema.org/"
  },
  "@graph": [
    {
      "@id": "_:42",
      "@type": "schema:Person",
      "schema:givenName": "John",
      "schema:familyName": "Doe",
      "schema:birthDate": "1978-05-17"
    }
  ]
}
```

# plugin configuration

The default settings are in `config.js`. They can be replaced with a `rdf.config.json` in the same directory as `index.js`.

The tag `@LinkedDataDescription` in the beginning of a JSDoc comment tells the plugin that a JSON-LD description should be generated for this comment, due to this setting:

```
encoderSettings: {
  createFor: { hasKey: 'LinkedDataDescription' },
```

This way more JSON-LD files can be generated from a single JavaScript file with separate comments. If you only want to generate one description per file and the file contains no other JSDoc annotations that should be excluded, you can choose `createFor: false` and don't have to use the tag anymore. Or set a file path or name as condition using the `path` and `filename` keys in the `createFor` object.

## tags

The default tag titles can be adjusted by changing the following settings:

```
"tagSettings": {
    "vocabTags": ["vocab", "context"],
    "subjectTag": "subj",
    "predicateTag": "pred",
    "objectTag": "obj",
}
```

Optionally you can define your own tag titles to combine the predicate and object tags by adding titles to the `propertyTags` option. In the example above we could replace the `@pred schema:givenName @obj John` annotation with `@schema:givenName John` by using the following settings:

```
"tagSettings": {
    "propertyTags": ["schema:givenName"],
},

```

The tags defined with this option will become a property of the preceding subject tag.

## default vocabulary

The vocabulary that will always be added to the context is given in `encoderSettings`. You can change the `vocab` property with the setting or add new vocabularies with the tag `@vocab <prefix>: <url>` in your description.

```
"encoderSettings": {
    "vocab": {
      "schema": "https://schema.org/"
    }
}
```

## converter

In the example above, the tag `@schema:givenName` can be shortened to `@givenName` by using the `vocabMap` setting. It will map any string in predicate or object position of the description.

```
"tagSettings": {
    "propertyTags": ["givenName"],
},
"converterSettings": {
    "vocabMap": {
        "givenName": "schema:givenName"
    },
    "flattenedFormat": true,
    "predNoNestedFormat": [],
}
```

The default is JSON-LD in flattened document form. Set the option to `false` for the nodes to be nested or combine the two options by giving predicate names to `predNoNestedFormat` that should remain flattened in an otherwise nested format.

## output

A JSON-LD description will be printed for every file that is parsed by JSDoc. You can print all descriptions to one file by changing `printPerDesc` to `false`. The default output path is a `plugin-rdf` directory in JSDoc's destination (if the plugin's template wasn't used). Change it by setting the `destination` directory (relative to JSDoc's destination).

```
"general": {
    "printPerDesc": true,
    "destination": "./plugin-rdf"
}
```

# Replace the converter

To change or replace the converter a `rdfConverter` module in the `converter` directory has to export a `convert(rdf)` function, where the `rdf` parameter that the function will receive is an intermediate RDF format of the form

```
{
  subject: {
    predicate: object
  }
}
```