/* eslint-disable linebreak-style */
/**
 * @module plugins/jsdoc-plugin-rdf
 */

const logger = require('jsdoc/util/logger');
const { config } = require('./config');
const { sep, getLastElement } = require('./utils');
const { rdfTagDefinitions } = require('./tags/rdfTags');
const { setDocletSubject, docletToRdf } = require('./rdf/rdfEncoder');
const { convertDocletProperties } = require('./rdf/rdfProperties');
const { convert } = require('./converter/rdfConverter');
const { printResults } = require('./converter/print');

const pluginname = 'jsdoc-plugin-rdf';

const configPath = () => {
  let pluginpath;
  try {
    pluginpath = require.resolve('./rdf.config.json');
  } catch (e) {
    console.log(`${pluginname}: using default config`);
  }
  return pluginpath;
};
const opts = config(configPath());

function defineTags(dictionary) {
  rdfTagDefinitions(dictionary);
}
/**
 * adds doc properties to rdf
 * @param {object} doc the doclet
 * @param {GenericRdf} rdf - the generic RDF instance
 */
function convertDoclet(doc, rdf) {
  const subjectName = setDocletSubject(doc);

  if (subjectName && rdf.addSubject(subjectName)) {
    convertDocletProperties(doc, subjectName, rdf);
  }
}

/**
* {@link https://jsdoc.app/about-plugins.html#event-handlers|JSDoc handler processingComplete}
*/
const handlers = {
  processingComplete(e) {
    const rdfResult = {};
    if (e.doclets) {
      e.doclets.forEach((doclet) => {
        if (doclet.name) {
          docletToRdf(rdfResult, doclet, convertDoclet);
        }
      });
    }
    if (rdfResult.result) {
      const rdfs = rdfResult.result;
      const converted = [];
      Object.keys(rdfs).forEach((rdfKey) => {
        const rdfInstance = rdfs[rdfKey];
        const result = convert(rdfInstance);
        const filename = getLastElement(rdfKey.split(`${sep}`));
        converted.push([filename, result]);
        if (rdfInstance.undefinedTags.length > 0) {
          logger.warn(`${pluginname}: Undefined tags used in ${filename}: ${rdfInstance.undefinedTags}`);
        }
      });
      printResults(converted);
    } else if (opts.encoderSettings.createFor) {
      logger.warn(`${pluginname}: No doclets were converted. The condition set in config.encoderSettings.createFor is ${opts.encoderSettings.createFor}`);
    } else {
      logger.warn(`${pluginname}: No doclets were converted.`);
    }
  },
};

module.exports = {
  defineTags,
  handlers,
};
