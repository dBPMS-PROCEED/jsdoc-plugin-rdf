/* eslint-disable linebreak-style */

const defaults = {
  general: {
    printPerDesc: true,
    destination: './plugin-rdf',
  },
  tagSettings: {
    vocabTags: [
      'vocab',
      'context',
      'prefix',
      'rdfns',
    ],
    predTags: ['LinkedDataDescription'],
    subjectTag: 'subj',
    predicateTag: 'pred',
    objectTag: 'obj',
    renameSubject: 'subjectId',
    propertyTags: [],
    metaTags: ['filename'],
    subjectProps: [
      'rdfsubject',
    ],
    remainNode: ['LinkedDataDescription'],
  },
  encoderSettings: {
    createFor: { hasKey: 'LinkedDataDescription' },
    vocab: {
      schema: 'https://schema.org/',
    },
    // blankNodeIriLongname: false, todo?
    kindToType: {},
    excludeKeys: [
      'comment',
      'undocumented',
      'longname',
      'name',
      'meta',
      'memberof',
      'scope',
      'tags',
    ],
    propertyCondition: [],
    includeUndocumented: false,
  },
  converterSettings: {
    vocabMap: {
      description: 'schema:description',
      defaultvalue: 'schema:defaultValue',
    },
    predNoNestedFormat: [],
    flattenedFormat: true,
    nestedIds: false,
  },
};

const fs = require('fs');

const { propertySettings } = require('./rdf/rdfProperties');
const { encoderSettings } = require('./rdf/rdfEncoder');
const { tagSettings } = require('./tags/rdfTags');
const { outputSettings } = require('./converter/print');
const { converterSettings } = require('./converter/JsonLdBuilder');

function updateSettings(confSet, userSet) {
  const settings = confSet;
  Object.keys(userSet).forEach((k) => {
    if (confSet[k] && typeof confSet[k] === typeof userSet[k]) {
      settings[k] = { ...confSet[k], ...userSet[k] };
    } else {
      settings[k] = userSet[k];
    }
  });
  return settings;
}

const config = (filepath, configSettings) => {
  const defaultSettings = configSettings || defaults;
  let opts = defaultSettings;
  let userSettings = {};
  if (filepath) {
    try {
      userSettings = JSON.parse(fs.readFileSync(filepath, 'utf8'));
      opts = userSettings ? updateSettings(defaultSettings, userSettings) : opts;
    } catch (e) {
      console.log('Error when parsing rdf.config. default settings were used', e);
    }
  }
  tagSettings(opts.tagSettings);
  encoderSettings(opts.encoderSettings, opts.tagSettings.renameSubject);
  propertySettings(opts.encoderSettings);
  converterSettings(opts.converterSettings);
  outputSettings(opts.general.destination, opts.general.printPerDesc);
  return opts;
};

module.exports = {
  config,
};
