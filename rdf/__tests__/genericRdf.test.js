/* eslint-disable linebreak-style */
/* eslint no-undef: "off" */

const { RdfResult, GenericRdf } = require('../genericRdf');
const config = require('../../config').setConfig();

const rdfResult = new RdfResult(config);
const rdf = new GenericRdf(config);

const plugin = require('../../index');

it('no doclets', () => {
  const e = {};
  plugin.handlers.processingComplete(e);
  expect(rdfResult.result).toEqual({});
});

it('addSubject', () => {
  rdf.addSubject('subj');
  expect(rdf.graph.subj).toEqual({});
});

it('addTriple', () => {
  rdf.addTriple('subj', 'pred', 'obj');
  expect(rdf.graph.subj).toEqual({ pred: 'obj' });
});

it('addTriple duplicate', () => {
  rdf.addTriple('subj', 'pred', 'obj');
  expect(rdf.graph.subj).toEqual({ pred: 'obj' });
});

/* todo test:
  - subjects have same name
  - none of the rdfDocument properties are objects, if array only literals
  - predicate is always a string
  - if property in typeMapping, it will be in rdfDocument type list
  - all subject objects are identified
*/
