/* eslint-disable linebreak-style */
/**
 * @module rdfEncoder
 */

const logger = require('jsdoc/util/logger');
const { checkConditions, getLastElement, sep } = require('../utils');
const { GenericRdf } = require('./genericRdf');

let subjectLong;
let vocab;
let includeUndocumented;
let createFor;
let allowDocletRef;
let rdfConditionKey;
let rename;

function encoderSettings(conf, renameSubject) {
  rename = renameSubject;
  subjectLong = conf.blankNodeIriLongname;
  createFor = conf.createFor;
  rdfConditionKey = (createFor && createFor.hasKey) ? createFor.hasKey : false;
  allowDocletRef = conf.allowDocletRef;
  vocab = conf.vocab;
  includeUndocumented = conf.includeUndocumented;
}

/**
 * sets the subject Id to be added to the GenericRdf instance
 * @param {object} doclet - the current doclet
 * @returns {string | boolean} the subject Id or false if subject shouldn't be added
 */
function setDocletSubject(doclet) {
  let subjectName = false;
  const defaultId = subjectLong ? doclet.longname : doclet.name;
  if (doclet[rename] && typeof doclet[rename] === 'string') {
    subjectName = doclet[rename]; // don't change doc.name in rdfTags already cause it shows in docu
  } else if (!includeUndocumented && !doclet.undocumented) {
    subjectName = defaultId;
  } else if (includeUndocumented) {
    subjectName = defaultId;
  }
  return subjectName;
}

/**
 * creates the GenericRdf instance the doclet properties will be added to
 * @param {object} rdfResult object to store the instances
 * @param {object} doclet doclet to check if a new instance should be created
 * @returns {GenericRdf} the created instance
 */
function setRdf(rdfResult, doclet) {
  const rdfs = rdfResult;
  const docfile = doclet.meta ? `${doclet.meta.path}${sep}${doclet.meta.filename}` : undefined;
  let instanceKey = docfile;
  if (!rdfs.result) {
    rdfs.result = {};
  }
  if (rdfConditionKey) {
    const name = doclet[rdfConditionKey].name || doclet.name;
    instanceKey = `${docfile}.${name}`;
  }
  if (instanceKey && !rdfs.result[instanceKey]) {
    rdfs.result[instanceKey] = new GenericRdf(vocab, docfile);
  }
  return rdfs.result[instanceKey];
}

/**
 * check if the doclet was referenced in an existing instance (if callback is described
 * by function without create condition tag, so callback will still be included)
 * @param {object} resultObj rdf instances
 * @param {string} docname current doclet.name
 * @returns {string} last instance that contain this doclet name, in the same file
 */
function containsDocletRef(resultObj, doclet) {
  let parentInstance = [];
  if (allowDocletRef && doclet.name && doclet.meta) {
    const docfile = `${doclet.meta.path}${sep}${doclet.meta.filename}`;
    Object.keys(resultObj).forEach((instance) => {
      if (docfile === resultObj[instance].file
        && resultObj[instance].subjectsAdded.includes(doclet.name)) {
        parentInstance.push(instance);
      }
    });
  }
  parentInstance = parentInstance.length > 0 ? getLastElement(parentInstance) : false;
  return parentInstance;
}

/**
 * starting point to convert a doclet with a plugin
 * @param {object} rdfResult object with GenericRdf instances
 * @param {object} doclet doclet to check create condition for
 * @param {function} convertDocletFunc callback that converts a doclet
 */
function docletToRdf(rdfResult, doclet, convertDocletFunc) {
  const createCondition = createFor
    ? checkConditions(createFor, doclet) : true;
  const partOf = rdfResult.result ? containsDocletRef(rdfResult.result, doclet) : false;
  if (createCondition || partOf) {
    // todo: add to each partOf instance?
    const rdf = createCondition ? setRdf(rdfResult, doclet) : rdfResult.result[partOf];
    if (rdfResult.result) {
      if (doclet.vocab) {
        if (typeof doclet.vocab === 'object') {
          rdf.vocab = { ...rdf.vocab, ...doclet.vocab };
        } else if (doclet.vocab.split(': ')[1]) {
          const prefix = doclet.vocab.split(': ')[0];
          // eslint-disable-next-line prefer-destructuring
          rdf.vocab[prefix] = doclet.vocab.split(': ')[1];
        }
      }
      if (doclet.tags) {
        doclet.tags.forEach((tag) => {
          if (!rdf.undefinedTags.includes(tag.originalTitle)) {
            rdf.undefinedTags.push(tag.originalTitle);
          }
        });
      }
      /* treat each node in rdfsubject like its own doclet
          this doclet only has the new subjects from a comment without code */
      if (doclet.rdfsubject) {
        doclet.rdfsubject.forEach((subjectNode) => {
          convertDocletFunc(subjectNode, rdf);
        });
      } else {
        convertDocletFunc(doclet, rdf);
      }
    } else {
      logger.warn('RDF plugin: A RDF instance was not be created');
    }
  }
}

module.exports = {
  encoderSettings,
  setDocletSubject,
  docletToRdf,
};
