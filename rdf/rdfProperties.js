/* eslint-disable linebreak-style */
/**
 * @module rdfProperties
 */

const logger = require('jsdoc/util/logger');
const { checkConditions, removeHtml } = require('../utils');

// to make a custom type refer to its typedef subject Id in the rdf graph
const customTypeMapping = {};

let includeKeys;
let excludeKeys;
let propertyCondition;
let kindToType;
let subjectLong;
let optionalToRequired;

function propertySettings(conf) {
  subjectLong = conf.blankNodeIriLongname;
  kindToType = conf.kindToType;
  includeKeys = conf.includeKeys || false;
  excludeKeys = Array.isArray(conf.excludeKeys)
    ? ['rdfsubject', 'vocab', 'rdfpred', 'LinkedDataDescription', 'subjectId', ...conf.excludeKeys] : false;
  propertyCondition = conf.propertyCondition;
  optionalToRequired = conf.optionalToRequired;
}

/**
 * Filters object keys based on config.includeKeys or excludeKeys
 * @param {object} obj the doclet or property object
 * @returns {string[]} the keys of obj that should be added to the genericRdf
 */
function defaultKeysToAdd(obj) {
  let keylist = [];
  if (includeKeys) {
    keylist = includeKeys;
  } else if (excludeKeys) {
    keylist = Object.keys(obj).filter((key) => !excludeKeys.includes(key));
  } else { keylist = Object.keys(obj); }
  return keylist;
}

/**
 * converts the {@link https://github.com/jsdoc/jsdoc/blob/master/lib/jsdoc/schema.js|TYPE_PROPERTY_SCHEMA},
 * adds everything in object's type.names list to the given subject's type property
 * @param {object} obj - doclet or PARAM_SCHEMA object
 * @param {string} subjectName - id for this subject
 * @param {GenericRdf} rdf
 */
function convertTypeSchema(obj, subjectName, rdf) {
  obj.type.names.forEach((t) => {
    const atType = customTypeMapping[t] ? customTypeMapping[t] : t;
    rdf.addTriple(subjectName, 'type', atType);
  });
}

/**
 * adds a new key-value pair to the subject based on conditions set in config.propertyCondition
 * @param {string} subject - subject id the properties will be added to if conditions are fulfilled
 * @param {object} doclet - current doclet or rdf node created in rdfEncoder
 * @param {GenericRdf} rdf - instance that has the subject
 */
function propertiesFromConfig(subject, doclet, rdf) {
  const predConditions = propertyCondition;
  try {
    if (predConditions && Array.isArray(predConditions)) {
      predConditions.forEach((predObj) => {
        if (Object.keys(predObj) && Object.keys(predObj).length === 2
          && Object.keys(predObj)[1] === 'conditions') {
          const pred = Object.keys(predObj)[0];
          if (doclet[pred] === undefined) { // don't overwrite existing properties with config
            const conditionObj = predObj.conditions;
            if (checkConditions(conditionObj, doclet)) {
              rdf.addTriple(subject, pred, predObj[pred]);
            }
          }
        } else {
          logger.warn('config.propertyCondition not defined correctly, see README');
        }
      });
    }
  } catch (e) {
    logger.warn(`Error when trying to add property from config.propertyCondition: ${e}`);
  }
}

/**
 * adds a new subject to GenericRdf if doclet property was type object
 * @param {object} obj the new subject node
 * @param {string} key the key that has obj as value
 * @param {string} parent obj's parent Id
 * @param {GenericRdf} rdf
 * @returns {string | boolean} Id of the new subject that was added to parent.key or false
 * if none was created
 */
function subjectFromDocletProperty(obj, key, parent, rdf) {
  let objId = false;
  let parentId = parent;
  let parentPred = key;
  if (obj.name) {
    if (obj.name.split('.')[1]) { // is property? always take last two names
      const properties = obj.name.split('.');
      parentId = properties[properties.length - 2];
      if (rdf.graph[parentId]) {
        objId = properties[properties.length - 1];
        parentPred = 'hasPart';
      }
    } else {
      objId = subjectLong ? `${parent}.${key}.${obj.name}` : obj.name;
    }
  } else if (rdf.graph[`${parent}.${key}`]) {
    const id = `${parent}.${key}`;
    const count = Array.isArray(rdf.graph[id]) ? rdf.graph[id].length : 2;
    objId = `${id}${count}`;
  } else {
    objId = `${parent}.${key}`;
  }

  if (objId) {
    rdf.addSubject(objId);
    if (kindToType && kindToType[key]) {
      rdf.addTriple(objId, 'type', kindToType[key]);
    }
    rdf.addTriple(parentId, parentPred, `_:${objId}`);
    if (parentPred === 'hasPart') {
      rdf.addTriple(parentId, 'type', 'complexType');
    }
  }
  return objId;
}

/**
 * adds doclet properties to a GenericRdf instance
 * @param {object} obj - doclet or its property object
 * @param {string} subjectName - the current subject Id
 * @param {GenericRdf} currentRdf - instance properties will be added to
 * @param {string[]} [ids] - list of ids that were created for this doclet
 * @returns {string[]} ids of subjects that were created, can be used in extensions
 */
function convertDocletProperties(obj, subjectName, currentRdf, ids) {
  const rdf = currentRdf;
  if (obj === undefined) {
    throw new Error(`${subjectName}'s object is undefined`);
  }
  if (Array.isArray(ids)) {
    ids.push(subjectName);
  }
  const keylist = defaultKeysToAdd(obj);
  keylist.forEach((key) => {
    switch (key) {
      case 'kind':
        if (obj.kind === 'typedef') {
          customTypeMapping[obj.name] = subjectName;
        }
        if (kindToType && kindToType[obj.kind]) {
          rdf.addTriple(subjectName, 'type', kindToType[obj.kind]);
        }
        break;
      case 'type':
        if (obj.type.names) {
          convertTypeSchema(obj, subjectName, rdf);
        } else {
          rdf.addTriple(subjectName, 'type', obj.type);
        }
        break;
      case 'defaultvalue':
        if (typeof obj.defaultvalue === 'number' && Number.isInteger(obj.defaultvalue)) {
          rdf.addTriple(subjectName, 'type', 'integer');
        } else { rdf.addTriple(subjectName, 'type', typeof obj.defaultvalue); }
        rdf.addTriple(subjectName, key, obj[key]);
        break;
      case 'optional':
        if (optionalToRequired) {
          rdf.addTriple(subjectName, 'required', false);
        } else {
          rdf.addTriple(subjectName, 'optional', obj[key]);
        }
        break;
      case 'description':
      case 'classdesc':
        rdf.addTriple(subjectName, 'description', removeHtml(obj[key]));
        break;
      default:
        if (Array.isArray(obj[key]) && obj[key].every((e) => typeof e === 'object')) {
          rdf.addTriple(subjectName, key, []);
          obj[key].forEach((o) => {
            const objId = subjectFromDocletProperty(o, key, subjectName, rdf);
            if (objId) {
              convertDocletProperties(o, objId, rdf, ids);
            }
          });
        } else if (!Array.isArray(obj[key]) && typeof obj[key] === 'object') {
          const objId = subjectFromDocletProperty(obj[key], key, subjectName, rdf);
          if (objId) {
            convertDocletProperties(obj[key], objId, rdf, ids);
          }
        } else {
          rdf.addTriple(subjectName, key, obj[key]);
        }
    }
  });
  if (obj.meta) { // in case of doclet specific condition
    propertiesFromConfig(subjectName, obj, rdf);
  } // use created subject instead doclet to compare conditions
  propertiesFromConfig(subjectName, rdf.graph[subjectName], rdf);
  return ids;
}

module.exports = {
  propertySettings,
  convertDocletProperties,
};
