/* eslint-disable linebreak-style */
/**
 * @module GenericRdf
 */

const logger = require('jsdoc/util/logger');
const fs = require('fs');
const { getLastElement, addToPred } = require('../utils');

/**
 * The generic RDF representation that will be converted to JSON-LD
 * @field vocab - the vocabulary
 * @field graph - contains all triples in the form 'subject: { predicate: object }'
 */
exports.GenericRdf = class GenericRdf {
  constructor(configVocab, file) {
    let vocabObj = {};
    this.file = file;
    this.subjectsAdded = [];
    this.subjectDouble = [];
    this.undefinedTags = [];
    if (typeof configVocab === 'object' && !Array.isArray(configVocab)) {
      vocabObj = configVocab;
    } else if (typeof configVocab === 'string') {
      try {
        vocabObj = JSON.parse(fs.readFileSync(configVocab, 'utf8'));
      } catch (e) { logger.warn(`could not parse vocab file in ${configVocab}`); }
    }
    this.vocab = { ...vocabObj };
    this.graph = {};
  }

  /**
   * adds a subject if the id doesn't exist already
   * @param {string} subjectId the subject Id
   * @returns {boolean} whether the subject was added/exists in graph
   */
  addSubject(subjectId) {
    let added = false;
    if (subjectId) {
      if (this.graph[subjectId] === undefined) {
        this.graph[subjectId] = {};
        added = true;
        this.subjectsAdded.push(subjectId);
      } else {
        added = true;
        if (!this.subjectDouble.includes(subjectId)) {
          this.subjectDouble.push(subjectId);
        }
      }
    } else {
      logger.warn(`subject Id '${subjectId}' not added. Last added subject '${getLastElement(this.subjectsAdded)}'.`);
    }
    return added;
  }

  /**
   * adds predicate and object to a subject
   * @param {string} subject - the subject Id
   * @param {string} predicate - the key to add
   * @param {any} object - the value to add
   */
  addTriple(subject, predicate, object) {
    if (typeof subject === 'string' && this.graph[subject]) {
      if (typeof predicate === 'string' && object !== undefined) {
        if (this.graph[subject][predicate] === undefined) {
          this.graph[subject][predicate] = object;
        } else {
        // if pred already has obj, combine with new value
          const currentObj = this.graph[subject][predicate];
          if (!(currentObj === object)) {
            this.graph[subject][predicate] = addToPred(currentObj, object);
          }
        }
      }
    } else {
      logger.warn(`addTriple: problem with '${subject}.${predicate}', tried adding '${object}'`);
    }
  }

  /**
   * to add a triple to the graph, only if subject doesn't exist yet
   * @param id - new subject Id
   * @param node - object containing predicate and object
   */
  addNode(id, node) {
    if (this.addSubject(id)) {
      this.graph[id] = node;
    }
  }
};
