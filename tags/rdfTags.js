/* eslint-disable linebreak-style */
/**
 * @module rdfTags
 */

const logger = require('jsdoc/util/logger');
const { getLastElement } = require('../utils');
const {
  currentSubject, addValueToPred, addToLatestPred, defineTagFromConfig,
} = require('./defineTagUtils');

let subjectTag;
let predicateTag;
let objectTag;
let vocabTags;
let predTags;
let propertyTags;
let metaTags;
let subjectProps;
let remainNode; // should remain a node also with just name
let rename;

function tagSettings(conf) {
  subjectTag = conf.subjectTag;
  predicateTag = conf.predicateTag;
  objectTag = conf.objectTag;
  vocabTags = conf.vocabTags;
  predTags = conf.predTags;
  propertyTags = conf.propertyTags;
  metaTags = conf.metaTags;
  subjectProps = conf.subjectProps;
  remainNode = [...subjectProps, ...conf.remainNode];
  rename = conf.renameSubject;
}

/**
 * adds 'rdfsubject' property to a doclet, with a list of all new subject nodes
 * that will be added to the generic rdf graph in rdfEncoder
 */
const subjectTagOptions = {
  mustHaveValue: true,
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    const doc = doclet;
    if (doc.rdfsubject === undefined) {
      doc.rdfsubject = [];
    }
    if (tag.value && tag.value.name) {
      const subjectNode = tag.value;
      if (doc.name === undefined) {
        doc.name = doclet.meta.filename.replace('.js', ''); // without this doclet won't be generated out of only comment
        doc.undocumented = true; // to not show up in docu
      }
      doc.rdfsubject.push(subjectNode);
    } else { logger.warn(`${tag.originalTitle} needs a name`); }
  },
};

const predicateTagOptions = {
  mustHaveValue: true,
  canHaveName: true,
  onTagged: (doclet, tag) => {
    const doc = currentSubject(subjectProps, doclet);
    if (doc.rdfpred === undefined) {
      doc.rdfpred = [];
    }
    doc.rdfpred.push(tag.value.name);
  },
};

const objectTagOptions = {
  mustHaveValue: true,
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    const obj = currentSubject(subjectProps, doclet);
    if (obj.rdfpred) {
      if (tag.value.type || tag.value.description) {
        addToLatestPred(obj, 'rdfpred', tag.value);
      } else {
        addToLatestPred(obj, 'rdfpred', tag.value.name);
      }
    } else {
      logger.error(`rdfTags: @${tag.originalTitle} ${tag.value.name} has no predicate.`);
    }
  },
};

/**
 * for predicate tag names defined in config.predTags, can skip @pred tag
 * // @tagtile tagvalue -> { tagtitle: tagvalue }
 */
const customPredOptions = {
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    const pred = tag.originalTitle;
    let obj = doclet;
    if (doclet.rdfsubject) {
      obj = getLastElement(doclet.rdfsubject);
    }
    if (tag.value) {
      if (tag.value.type || tag.value.description || remainNode.includes(pred)) {
        addValueToPred(obj, pred, tag.value);
      } else if (tag.value.name) {
        addValueToPred(obj, pred, tag.value.name);
      }
    } else {
      addValueToPred(obj, pred, true);
    }
  },
};

const propTagOptions = {
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    const pred = tag.originalTitle;
    const obj = currentSubject(subjectProps, doclet);
    if (tag.value) {
      if (tag.value.type || tag.value.description || remainNode.includes(pred)) {
        addValueToPred(obj, pred, tag.value);
      } else if (tag.value.name) {
        addValueToPred(obj, pred, tag.value.name);
      }
    } else {
      addValueToPred(obj, pred, true);
    }
  },
};


const vocabOptions = {
  mustHaveValue: true,
  onTagged: (doclet, tag) => {
    const doc = doclet;
    if (tag.text[0] === '{') {
      try {
        doc.vocab = JSON.parse(tag.text);
      } catch (e) {
        logger.warn(`the tag value for @${tag.title} is not a valid JSON string`);
      }
    } else {
      doc.vocab = tag.value;
    }
    if (!doc.name) {
      doc.name = doclet.meta.filename.replace('.js', '');
      doc.undocumented = true;
    }
  },
};

const metaTagOptions = {
  onTagged: (doclet, tag) => {
    const pred = tag.originalTitle;
    let obj = doclet;
    if (doclet.rdfsubject) {
      obj = getLastElement(doclet.rdfsubject);
    }
    if (obj[pred] === undefined) {
      obj[pred] = doclet.meta[pred];
    }
  },
};

function rdfTagDefinitions(dictionary) {
  defineTagFromConfig(dictionary, subjectTag, subjectTagOptions);
  defineTagFromConfig(dictionary, predicateTag, predicateTagOptions);
  defineTagFromConfig(dictionary, objectTag, objectTagOptions);
  defineTagFromConfig(dictionary, vocabTags, vocabOptions);
  if (metaTags) {
    defineTagFromConfig(dictionary, metaTags, metaTagOptions);
  }
  if (predTags) {
    defineTagFromConfig(dictionary, predTags, customPredOptions);
  }
  if (Array.isArray(propertyTags)) {
    defineTagFromConfig(dictionary, [...propertyTags, rename], propTagOptions);
  }
}

module.exports = {
  tagSettings,
  rdfTagDefinitions,
};
