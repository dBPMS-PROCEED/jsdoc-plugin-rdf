/* eslint-disable linebreak-style */
/**
 * @module tagUtils
 */

const logger = require('jsdoc/util/logger');
const { getLastElement, addToPred } = require('../utils');

function currentSubject(subjNodes, doclet) {
  let doc = doclet;
  for (let i = 0; i < subjNodes.length; i += 1) {
    if (doclet[subjNodes[i]]) {
      doc = getLastElement(doclet[subjNodes[i]]);
      break;
    }
  }
  return doc;
}

/**
 * @param {object} obj current subject
 * @param {string} pred key to add
 * @param {any} value the value to add to obj.pred
 */
function addValueToPred(obj, pred, value) {
  const doc = obj;
  if (doc[pred] === undefined) {
    doc[pred] = value;
  } else {
    const currentVal = doc[pred];
    doc[pred] = addToPred(currentVal, value);
  }
}

/**
 * add value to the last predicate and predicate to the last subject
 * @param {object} obj the current subject
 * @param {string} key rdfpred or rdfsubject key of the doclet
 * @param {object | string} valueToAdd value to add to key
 */
function addToLatestPred(obj, key, valueToAdd) {
  const doc = obj;
  const latest = getLastElement(doc[key]);
  if (latest !== null) {
    addValueToPred(doc, latest, valueToAdd);
  }
}

/**
 * defines tagOpts for all titles in tags
 * @param dictionary JSDoc defineTags dictionary
 * @param {array} tags the tagtitles
 * @param {object} tagOpts the options object to define the tag with
 */
function defineTagFromConfig(dictionary, tags, tagOpts) {
  if (tags) {
    const allow = ['return'];
    const defined = [];
    const taglist = Array.isArray(tags) ? tags : [tags];
    taglist.forEach((tag) => {
      if (tag) {
        if (dictionary.lookUp(tag) === false || allow.includes(tag)) {
          dictionary.defineTag(tag, tagOpts);
          defined.push(tag);
        } else {
          logger.warn(`rdfTags: ${tag} tag is already defined in dictionary`);
        }
      }
    });
  }
}

module.exports = {
  currentSubject,
  addToLatestPred,
  addValueToPred,
  defineTagFromConfig,
};
