/* eslint-disable linebreak-style */

const logger = require('jsdoc/util/logger');
const path = require('path');

const conditionMetaKeys = ['path', 'filename'];

const { sep } = path;

function confirmVocabPrefix(contextObj, vocabref, missing) {
  if (vocabref.split(':')[1] && vocabref.split(':')[0] !== '_') {
    const prefix = vocabref.split(':')[0];
    if (!contextObj[prefix] && !missing.includes(prefix)) {
      missing.push(prefix);
    }
  }
}

function removeHtml(str) {
  let strVal = str.replace(/<p>|<\/p>/g, '');
  strVal = strVal.replace(/\n|\r/g, ' ');
  return strVal;
}

function removeDuplicate(li) {
  let list = li;
  list = list.filter((item, index) => list.indexOf(item) === index);
  return list;
}

/**
 * adds a new value to a key that already has a value
 * @param {any} predVal - the current value
 * @param {any} newVal - the new value to add
 * @returns {any} the result of combining the two values
 */
function addToPred(predVal, newVal) {
  let returnVal = [];
  if (typeof predVal !== 'boolean') {
    if (!Array.isArray(predVal)) {
      returnVal = !Array.isArray(newVal) ? [predVal, newVal] : [predVal, ...newVal];
    } else {
      returnVal = !Array.isArray(newVal) ? [...predVal, newVal] : [...predVal, ...newVal];
    }
    returnVal = removeDuplicate(returnVal);
  } else {
    returnVal = newVal;
    logger.warn(`${predVal} was replaced with ${newVal}!`);
  }
  return returnVal;
}

function getLastElement(list) {
  let element = list;
  if (Array.isArray(list) && list.length > 0) {
    element = list[list.length - 1];
  }
  return element;
}

/**
 * get the name of the function this doclet belongs to from its memberof property
 * @param {object} doc - the doclet of the member
 * @returns {string} the name of the function that contains the member
 */
function getMembersFunctionName(doc) {
  const memberofArray = doc.memberof.split('.');
  let argsId = null;
  if (memberofArray.length > 1) {
    argsId = memberofArray[memberofArray.length - 1]; // just the function name
  } else {
    argsId = doc.memberof;
  }
  return argsId;
}

function conditionCheck(conditionKey, conditionVal, doc) {
  let bool = false;
  let docVal = doc[conditionKey];
  if (conditionKey === 'type' && doc.type) {
    docVal = doc.type.names ? doc.type.names : doc.type;
    bool = docVal.includes(conditionVal);
  } else if (conditionMetaKeys
      && conditionMetaKeys.includes(conditionKey) && doc.meta) {
    if (conditionKey === 'path') {
      bool = doc.meta.path.includes(`${sep}${conditionVal}`);
    } else {
      bool = doc.meta[conditionKey] === conditionVal;
    }
  } else if (conditionKey === 'hasKey') {
    bool = !!doc[conditionVal];
  } else if (conditionKey === 'noKey') {
    bool = !doc[conditionVal];
  } else if (Array.isArray(conditionVal)) {
    bool = conditionVal.includes(docVal);
  } else {
    bool = conditionVal === docVal;
  }
  return bool;
}

/**
 * checks if the current doclet or rdf subject fulfills all conditions in the condition object
 * @param {object} conditionObj the condition object
 * @param {object} doc the doclet or rdf subject
 * @returns {boolean} true if all conditions matched
 */
function checkConditions(conditionObj, doc) {
  let bool = false;
  if (conditionObj && Object.keys(conditionObj).length > 0) {
    const conditions = Object.keys(conditionObj);
    bool = conditions.every(
      (conditionKey) => conditionCheck(conditionKey, conditionObj[conditionKey], doc),
    );
  }
  return bool;
}

module.exports = {
  sep,
  confirmVocabPrefix,
  removeDuplicate,
  addToPred,
  getLastElement,
  checkConditions,
  getMembersFunctionName,
  removeHtml,
};
