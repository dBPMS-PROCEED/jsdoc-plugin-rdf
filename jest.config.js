/* eslint-disable linebreak-style */
// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  moduleNameMapper: {
    'jsdoc/util/logger': 'jsdoc/lib/jsdoc/util/logger',
    'jsdoc/env': '<rootDir>./node_modules/jsdoc/lib/jsdoc/env',
  },
};
