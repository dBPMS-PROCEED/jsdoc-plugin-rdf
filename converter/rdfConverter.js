/* eslint-disable linebreak-style */
/**
 * @module rdfConverter
 */

const logger = require('jsdoc/util/logger');
const { JsonLdBuilder } = require('./JsonLdBuilder');

/**
 * @param {GenericRdf} rdf instance
 * @returns JSON-LD description
 */
function convert(rdf) {
  const builder = new JsonLdBuilder(rdf);
  builder.subjects.forEach((subjectId) => {
    let graphNode;
    if (!builder.flattenedFormat) {
      graphNode = builder.convertNested(subjectId);
    } else {
      graphNode = builder.convertFlat(subjectId);
    }
    if (graphNode) {
      builder.jsonLd['@graph'].push(graphNode);
    }
  });
  if (builder.missingContext.length > 0) {
    logger.warn(`no @context entry for prefixes ${builder.missingContext} used in ${rdf.file}`);
  }
  return builder.jsonLd;
}

module.exports = {
  convert,
};
