/* eslint-disable linebreak-style */
/**
 * @module print
 */

const { opts } = require('jsdoc/env');
const logger = require('jsdoc/util/logger');
const fs = require('fs');
const path = require('path');
const { sep } = require('../utils');

let destination;
let printPerDesc;

function outputSettings(out, print) {
  if (opts.template && opts.template.includes('plugin-rdf-template')) {
    destination = opts.destination;
  } else {
    destination = path.join(opts.destination, out);
  }
  printPerDesc = print;
}

function printResult(result, outPath, outfile) {
  const jsonldString = JSON.stringify(result, null, 2);
  if (!fs.existsSync(outPath)) {
    fs.mkdirSync(outPath, { recursive: true }, (err) => {
      if (err) {
        logger.error(`Couldn't create directory ${outPath}`, err);
      }
    });
  }
  const filepath = `${outPath}${sep}${outfile}`;
  fs.writeFile(`${filepath.replace('.js', '')}.jsonld`, jsonldString, (err) => {
    if (err) {
      logger.error(`Couldn't print JSON-LD to ${outPath}`, err);
    } else {
      console.log(`JSON-LD ${outfile.replace('.js', '')} generated in ${outPath}`);
    }
  });
}

function printResults(results) {
  const outPath = destination;
  const resultCollection = [];
  results.forEach((result) => { // result = [filename, jsonld]
    if (!printPerDesc) {
      resultCollection.push({
        semanticDescription: result[1],
        identifier: `id${resultCollection.length + 1}`,
      });
    } else {
      printResult(result[1], outPath, result[0]);
    }
  });
  if (resultCollection.length > 0) {
    printResult(resultCollection, outPath, 'semanticDescription');
  }
}

module.exports = {
  outputSettings,
  printResults,
};
