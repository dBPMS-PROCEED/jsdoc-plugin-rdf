/* eslint-disable linebreak-style */

const { confirmVocabPrefix, removeDuplicate } = require('../utils');

let vocabMap;
let flattenedFormat;
let predNoNestedFormat;
let nestedIds;

function converterSettings(conf) {
  vocabMap = conf.vocabMap || {};
  flattenedFormat = conf.flattenedFormat;
  predNoNestedFormat = conf.predNoNestedFormat;
  nestedIds = conf.nestedIds;
}

function blankNodeIri(name) {
  return `_:${name}`;
}

/**
 * converts GenericRdf instance to JSON-LD
 * @field jsonLd - converted JSON-LD object to be serialized
 */
class JsonLdBuilder {
  constructor(genericRdf) {
    this.rdfGraph = genericRdf.graph;
    this.subjects = Object.keys(this.rdfGraph);
    this.missingContext = [];
    this.flattenedFormat = flattenedFormat;
    if (!flattenedFormat) {
      this.nestedSubjects = [];
    }
    this.jsonLd = {
      '@context': genericRdf.vocab,
      '@graph': [],
    };
  }

  /**
  * converts GenericRdf properties to JSON-LD syntax and adds them to a JSON-LD graph node
  * @param {string} predicate the rdf predicate
  * @param {any} objValue the rdf object
  * @param {object} newNode the object the properties will be added to
  */
  convertProperty(predicate, objValue, newNode) {
    const graphNode = newNode;
    if (predicate === 'type') {
      graphNode['@type'] = Array.isArray(objValue)
        ? this.mapIdList(objValue)
        : this.mapId(objValue);
    } else {
      let mapTag = predicate;
      mapTag = vocabMap[predicate] || mapTag;
      confirmVocabPrefix(this.jsonLd['@context'], mapTag, this.missingContext);
      if (Array.isArray(objValue)) {
        graphNode[mapTag] = this.convertArray(predicate, objValue);
      } else if (!Array.isArray(objValue) && typeof objValue === 'object') {
        graphNode[mapTag] = this.convertNestedObj(objValue);
      } else {
        graphNode[mapTag] = this.mapValue(predicate, objValue);
      }
    }
  }

  /**
   * creates a graph node containing all its nested nodes
   * @param {string} subjectId
   * @returns JSON-LD graph node
   */
  convertNested(subjectId) {
    let graphNode;
    if (!this.nestedSubjects.includes(subjectId)) {
      graphNode = { '@id': blankNodeIri(subjectId) };
      Object.keys(this.rdfGraph[subjectId]).forEach((predicate) => {
        this.convertProperty(predicate, this.rdfGraph[subjectId][predicate], graphNode);
      });
    }
    return graphNode;
  }

  /**
   * creates a graph node for each subject Id
   * @param {string} subjectId
   * @returns JSON-LD graph node
   */
  convertFlat(subjectId) {
    const graphNode = { '@id': blankNodeIri(subjectId) };
    Object.keys(this.rdfGraph[subjectId]).forEach((predicate) => {
      this.convertProperty(predicate, this.rdfGraph[subjectId][predicate], graphNode);
    });
    return graphNode;
  }

  convertNestedObj(obj) {
    const nestedObj = {};
    Object.keys(obj).forEach((pred) => {
      this.convertProperty(pred, obj, nestedObj);
    });
    return nestedObj;
  }

  /**
    * converts array type and checks if contents need a blank node identifier
    * @param {array} arrayVal array to be converted
    * @returns {object} object with '@list' property, containing strings or ids
    */
  convertArray(predicate, arrayVal) {
    const returnList = { '@list': [] };
    arrayVal.forEach((val) => {
      let objValue = val;
      if (typeof objValue === 'object') {
        objValue = this.convertNestedObj(objValue);
      }
      returnList['@list'].push(this.mapValue(predicate, objValue));
    });
    returnList['@list'] = removeDuplicate(returnList['@list']);
    return returnList;
  }

  resolveId(id, key) {
    let graphNode = (nestedIds && typeof nestedIds === 'boolean') ? { '@id': blankNodeIri(id) } : {};
    const idFor = Array.isArray(nestedIds) ? nestedIds : false;
    if (idFor && idFor.includes(key)) {
      graphNode = { '@id': blankNodeIri(id) };
    }
    Object.keys(this.rdfGraph[id]).forEach((predicate) => {
      this.convertProperty(predicate, this.rdfGraph[id][predicate], graphNode);
    });
    this.nestedSubjects.push(id);
    return graphNode;
  }

  /**
     * checks if the current rdf object is also a subject or has a name mapping
     * @param {string | number | boolean | null} val the rdf object
     * @returns {any} id ref object or resolved id (if nested format)
     * or the mapped val name or val unchanged
     */
  mapValue(predicate, val) {
    let returnElem = val;
    if (typeof val === 'string') { // && this.rdfGraph[val]) {
      const idRef = val.split(':');
      if (idRef.length === 2) {
        const prefix = idRef[0];
        if (prefix === '_') { // blank node IRI
          if (!flattenedFormat && !predNoNestedFormat.includes(predicate)) {
            returnElem = this.resolveId(idRef[1], predicate); // returns converted rdf node
          } else {
            returnElem = { '@id': val };
          }
        // returnElem = { '@id': `${blankNodeIri(val)}` };
        } else if (this.jsonLd['@context'][prefix]) { // vocab IRI
          returnElem = { '@id': val };
        } else if (!this.missingContext.includes(prefix)) {
          this.missingContext.push(prefix);
        }
      } else { returnElem = vocabMap[val] ? vocabMap[val] : val; }
    }
    return returnElem;
  }

  /**
     * like mapValue but the value's property already has type '@id'
     * @param {string} val value that is an Id
     */
  mapId(val) {
    let returnVal = '';
    if (this.rdfGraph[val]) {
      returnVal = blankNodeIri(val);
    } else {
      returnVal = vocabMap[val] ? vocabMap[val] : val;
      confirmVocabPrefix(this.jsonLd['@context'], returnVal, this.missingContext);
    }
    return returnVal;
  }

  /**
    * checks if values need blank node Iris or name mapping
    * @param {array} list value list to check
    * @returns the converted value list
  */
  mapIdList(list) {
    // like mapValue function but can't use one for everything cause @type list has no @id:
    let idList = [];
    list.forEach((val) => idList.push(this.mapId(val)));
    idList = removeDuplicate(idList);
    return idList;
  }
}

module.exports = {
  converterSettings,
  JsonLdBuilder,
};
