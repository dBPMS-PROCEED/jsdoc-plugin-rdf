/**
@LinkedDataDescription example
@vocab dctype: http://purl.org/dc/dcmitype/

@subj {schema:Person} JohnDoe description text
@pred schema:givenName
@obj John
@pred schema:familyName
@obj Doe
@pred schema:birthDate
@obj 1978-05-17

@subj {schema:Person} JaneDoe
@givenName Jane
@familyName Doe
@spouse _:JohnDoe
*/